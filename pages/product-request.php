<? require 'partials/header.php';?>

<h1>FMG Products</h1>

  <div id="alloptions">
    <div class="row">
      <div class="col-4" v-for="product in selectedProducts" id="products">
        <div class="card" id="product-card-">
          <div class="card-body">
            <div class="card-img-top">
              <img class="img-fluid" src="{{ product.images }}" alt="{{ product.name }}">
              <h2 class="card-title">{{ product.name }}</h2>
              <input id="product-check-" type="checkbox" value="{{ product.name }}">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--
    REFERENCE
    <p class="col-12">{{ product.name }}</p>
        <p class="i-have-been-iffed col-3" v-if="product.sizes.fiveHundred.length > 0" v-for="index in product.sizes.fiveHundred.length">
          {{ product.sizes.fiveHundred[index]}}
        </p>
        <div class="col-12" v-for="index in product.sizes.sixHundred.oneFifty.weight.length">
          <p>{{ product.sizes.sixHundred.oneFifty.weight[index] }}</p>
          <input v-bind:value="product.sizes.sixHundred.oneFifty.weight[index]" type="checkbox">
        </div>
        <img class="col-12 img-responsive" src="{{product.images}}" alt="image">
-->

  <?php include 'email.php';?>


  <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.25/vue.min.js'></script>
  <script src="/scripts/products.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous">
  </script>
  <script src="/scripts/script.js"></script>
  <script src="/scripts/product-pops.js"></script>

  </body>

  </html>