// need to add fucntion for checkbox reset on master change

// Size card height match
/*
var sizeCardHeight = 235;

for (i = 1; i <= 40; i++) {
  var toMatch = "#size-card-" + i;
  $(toMatch).css("height", sizeCardHeight);
}

for (i = 1; i <= 40; i++) {
  var toMatch = "#LX-size-card-" + i;
  $(toMatch).css("height", sizeCardHeight);
}

for (i = 1; i <= 40; i++) {
  var toMatch = "#FMR-size-card-" + i;
  $(toMatch).css("height", sizeCardHeight);
}
*/

$(document).ready(function() {

var checkboxes = $("#alloptions input[type='checkbox']");


checkboxes.on("change", function() {
  $("#fulloptions").val(
    checkboxes
    .filter(":checked")
    .map(function(item) {
      return this.value;
    })
    .get()
    .join(", ")
  );
});


// Allow card click to toggle checkbox.
$('.check-card').click(function() {
  $(this).find('input[type=checkbox]').prop('checked', function() {
    //    return !this.checked;  ######breaks filter
  });
});

//Functions to display selected options for initial selection
function sizeSelect1(clicked, toHide, exception) {
  $("#" + clicked + 1).click(function() {
    $("." + toHide).not($("#" + exception + 1)).toggleClass("disabled-grey");
    $(".eighty-options-1").toggleClass("hidden");
    console.log(clicked + 1 + " was clicked.");
  });
  $("#" + clicked + 2).click(function() {
    $("." + toHide).not($("#" + exception + 2)).toggleClass("disabled-grey");
    $(".one-hundred-options-1").toggleClass("hidden");
    console.log(clicked + 2 + " was clicked.");
  });
  $("#" + clicked + 3).click(function() {
    $("." + toHide).not($("#" + exception + 3)).toggleClass("disabled-grey");
    $(".one-fifty-options-1").toggleClass("hidden");
    console.log(clicked + 3 + " was clicked.");
  });
  $("#" + clicked + 4).click(function() {
    $("." + toHide).not($("#" + exception + 4)).toggleClass("disabled-grey");
    $(".two-hundred-options-1").toggleClass("hidden");
    console.log(clicked + 4 + " was clicked.");
  });
  $("#" + clicked + 5).click(function() {
    $("." + toHide).not($("#" + exception + 5)).toggleClass("disabled-grey");
    $(".two-fifty-options-1").toggleClass("hidden");
    console.log(clicked + 5 + " was clicked.");
  });
};

function sizeSelect2(clicked, toHide, exception) {
  $("#" + clicked + 1).click(function() {
    $("." + toHide).not($("#" + exception + 1)).toggleClass("disabled-grey");
    $(".eighty-options-2").toggleClass("hidden");
    console.log(clicked + 1 + " was clicked.");
  });
  $("#" + clicked + 2).click(function() {
    $("." + toHide).not($("#" + exception + 2)).toggleClass("disabled-grey");
    $(".one-hundred-options-2").toggleClass("hidden");
    console.log(clicked + 2 + " was clicked.");
  });
  $("#" + clicked + 3).click(function() {
    $("." + toHide).not($("#" + exception + 3)).toggleClass("disabled-grey");
    $(".one-fifty-options-2").toggleClass("hidden");
    console.log(clicked + 3 + " was clicked.");
  });
  $("#" + clicked + 4).click(function() {
    $("." + toHide).not($("#" + exception + 4)).toggleClass("disabled-grey");
    $(".two-hundred-options-2").toggleClass("hidden");
    console.log(clicked + 4 + " was clicked.");
  });
  $("#" + clicked + 5).click(function() {
    $("." + toHide).not($("#" + exception + 5)).toggleClass("disabled-grey");
    $(".two-fifty-options-2").toggleClass("hidden");
    console.log(clicked + 5 + " was clicked.");
  });
};

function sizeSelect3(clicked, toHide, exception) {
  $("#" + clicked + 1).click(function() {
    $("." + toHide).not($("#" + exception + 1)).toggleClass("disabled-grey");
    $(".eighty-options-3").toggleClass("hidden");
    console.log(clicked + 1 + " was clicked.");
  });
  $("#" + clicked + 2).click(function() {
    $("." + toHide).not($("#" + exception + 2)).toggleClass("disabled-grey");
    $(".one-hundred-options-3").toggleClass("hidden");
    console.log(clicked + 2 + " was clicked.");
  });
  $("#" + clicked + 3).click(function() {
    $("." + toHide).not($("#" + exception + 3)).toggleClass("disabled-grey");
    $(".one-fifty-options-3").toggleClass("hidden");
    console.log(clicked + 3 + " was clicked.");
  });
  $("#" + clicked + 4).click(function() {
    $("." + toHide).not($("#" + exception + 4)).toggleClass("disabled-grey");
    $(".two-hundred-options-3").toggleClass("hidden");
    console.log(clicked + 4 + " was clicked.");
  });
  $("#" + clicked + 5).click(function() {
    $("." + toHide).not($("#" + exception + 5)).toggleClass("disabled-grey");
    $(".two-fifty-options-3").toggleClass("hidden");
    console.log(clicked + 5 + " was clicked.");
  });
};

// sets click functions to relative product
var select = function testing(clicked, toHide, exception) {
  var _loop = function _loop(i) {
    $("#" + clicked + i).click(function() {
      $("." + toHide).not($("#" + exception + i)).toggleClass("disabled-grey");
      console.log(clicked + i + " was clicked.");
    });
  };

  for (var i = 0; i < 2000; i++) {
    _loop(i);
  }
};


var popIds = function popIds(check, card) {
  //set check- id's
  for (var i = 1; i < 10000; i++) {
    $("#" + check).attr("id", check + i);
  }

  //set card id's
  for (var _i = 1; _i < 10000; _i++) {
    $("#" + card).attr("id", card + _i);
  }
};

popIds('product-check-', 'product-card-');

popIds('g-check-', 'g-card-');

popIds('q-max-check-', 'q-max-card-');

popIds('q-min-check-air-', 'q-min-card-air-');

popIds('q-min-check-4-bar-', 'q-min-card-4-bar-');

popIds('q-min-check-8-bar-', 'q-min-card-8-bar-');

popIds('flange-length-check-', 'flange-length-card-');

popIds('body-length-check-', 'body-length-card-');

popIds('weight-check-', 'weight-card-');

popIds('quantity-check-', 'quantity-card-');


//FMT-S Options
sizeSelect2('size-check-', 'size-card', 'size-card-');

select('g-check-', 'g-card', 'g-card-');

select('q-max-check-', 'q-max-card', 'q-max-card-');

select('q-min-check-air-', 'q-min-card', 'q-min-card-air-');

select('q-min-check-4-bar-', 'q-min-card', 'q-min-card-4-bar-');

select('q-min-check-8-bar-', 'q-min-card', 'q-min-card-8-bar-');

select('flange-length-check-', 'flange-card', 'flange-length-card-');

select('body-length-check-', 'body-card', 'body-length-card-');

select('weight-check-', 'weight-card', 'weight-card-');

select('quantity-check-', 'quantity-card', 'quantity-card-');

// FMT-LX Options
sizeSelect3('LX-size-check-', 'LX-size-card', 'LX-size-card-');


// FMR Options
sizeSelect1('FMR-size-check-', 'FMR-size-card', 'FMR-size-card-');


var productOne = $("#product-card-1");
var productTwo = $("#product-card-2");
var productThree = $("#product-card-3");

var productCheckOne = $("#product-check-1");
var productCheckTwo = $("#product-check-2");
var productCheckThree = $("#product-check-3");

productCheckOne.click(function() {
    if ($(this).is(":checked")) {
      console.log("checked");
      /*
      $(".FMR").removeClass("hidden");
      */
      productTwo.addClass("disabled-grey");
      productThree.addClass("disabled-grey");
      productOne.addClass("selected");
    } else {
      /*
      $(".FMR").addClass("hidden");
      */
      productTwo.removeClass("disabled-grey");
      productThree.removeClass("disabled-grey");
      productOne.removeClass("selected");
    }
});

productCheckTwo.click(function() {
  if ($(this).is(":checked")) {
    console.log("checked");
    /*
    $(".FMR").removeClass("hidden");
    */
    productOne.addClass("disabled-grey");
    productThree.addClass("disabled-grey");
    productTwo.addClass("selected");
  } else {
    /*
    $(".FMR").addClass("hidden");
    */
    productOne.removeClass("disabled-grey");
    productThree.removeClass("disabled-grey");
    productTwo.removeClass("selected");
  }
});

productCheckThree.click(function() {
  if ($(this).is(":checked")) {
    console.log("checked");
    /*
    $(".FMR").removeClass("hidden");
    */
    productOne.addClass("disabled-grey");
    productTwo.addClass("disabled-grey");
    productThree.addClass("selected");
  } else {
    /*
    $(".FMR").addClass("hidden");
    */
    productOne.removeClass("disabled-grey");
    productTwo.removeClass("disabled-grey");
    productThree.removeClass("selected");
  }
});

})
