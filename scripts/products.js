var products = [
  {
    name: "FMG FS",
    sizes: {
      sixHundred: {
        oneFifty: {
          weight: [
            "1kg",
            "8kg",
            "12kg"
          ]
        }
      }
    },
    images:
      "https://placeimg.com/640/480/any",
    weights: 140000
  },
  {
    name: "FMG FG",
    sizes: {
      fiveHundred: [5, 12, 53, 64],
      sixHundred: "im six hundred",
      sixHundred: "im eight hundred"
    },
    images:
      "https://placeimg.com/640/480/any",
    weights: 140000
  },
  {
    name: "FMG FMR",
    sizes: {
      fiveHundred: [5, 12, 53, 64],
      sixHundred: "im six hundred",
      sixHundred: "im eight hundred"
    },
    images:
      "https://placeimg.com/640/480/any",
    weights: 140000
  }
];

var sortBy = function(arr, key) {
  return arr.sort(function(a, b) {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;
    return 0;
  });
};

var filterBy = function(arr, traits) {
  var length = traits.length;

  return arr.filter(function(item) {
    var i = 0;

    while (i < length) {
      if (item.indexOf(traits[i]) === -1) return false;
      i++;
    }

    return true;
  });
};

new Vue({
  el: "#products",
  data: {
    products: products,
    sort: "name"
  },
  computed: {
    selectedProducts: function() {
      var products = this.products;

      return sortBy(products, this.sort);
    }
  }
});

//remove whitespace only text nodes
function clean(node)
{
  for(var n = 0; n < node.childNodes.length; n ++)
  {
    var child = node.childNodes[n];
    if
    (
      child.nodeType === 8
      ||
      (child.nodeType === 3 && !/\S/.test(child.nodeValue))
    )
    {
      node.removeChild(child);
      n --;
    }
    else if(child.nodeType === 1)
    {
      clean(child);
    }
  }
}

clean(document);